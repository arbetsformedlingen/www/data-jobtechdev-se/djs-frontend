FROM nginx:1.27.4-bookworm

COPY nginx/default.conf.template /etc/nginx/templates/default.conf.template

RUN    ln -sf /dev/stdout    /var/log/nginx/nginx-access.log \
    && ln -sf /dev/stderr    /var/log/nginx/nginx-error.log
EXPOSE 8080
STOPSIGNAL SIGQUIT

ENV S3HOST s3-website.eu-central-1.amazonaws.com \
    BUCKET data.jobtechdev.se-adhoc-test

CMD ["nginx", "-g", "daemon off; error_log stderr info;"]
